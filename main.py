#!/usr/bin/env python3

"""
Copyright (C) 2020  Roberto Rosario <roberto.rosario.gonzalez@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the:
Free Software Foundation, Inc.
51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA.
"""

import argparse
import csv
import datetime
import decimal
import itertools


class Trade:
    ACTION_BUY = 1
    ACTION_SELL = 2
    ACTION_EXPIRATION = 3
    ACTION_DISPLAY = {
        ACTION_BUY: '+',
        ACTION_SELL: '-',
        ACTION_EXPIRATION: '(expired)',
    }

    POSITION_OPEN = 1
    POSITION_CLOSE = 2

    TYPE_CALL = 1
    TYPE_PUT = 2
    TYPE_DISPLAY = {
        TYPE_CALL: 'Call',
        TYPE_PUT: 'Put',
    }

    def __init__(self, row):
        # Trade Date
        # 2020-05-11
        # 0123456789

        # Symbol
        # SPXW  20200511P 2835.000
        # 012345678901234567890123

        # Timestamp
        # 2020-05-06T10:16:26.000
        # 01234567890123456789012

        expiration_year = int(row['Symbol'][6:10])
        expiration_month = int(row['Symbol'][10:12])
        expiration_day = int(row['Symbol'][12:14])

        self.expiration = datetime.date(
            year=expiration_year, month=expiration_month,
            day=expiration_day
        )

        self.net_amount = decimal.Decimal(row['Net Amount'][1:])

        if row['Trade Action'].endswith('OPEN'):
            self.position = Trade.POSITION_OPEN
        elif row['Trade Action'].endswith('CLOSE'):
            self.position = Trade.POSITION_CLOSE
        else:
            self.position = None

        self.price = float(row['Price'][1:])

        self.quantity = int(row['Qty'])

        self.strike = decimal.Decimal(row['Symbol'][16:24])

        self.symbol = row['Symbol'][0:4]

        trade_date_year = int(row['Timestamp'][0:4])
        trade_date_month = int(row['Timestamp'][5:7])
        trade_date_day = int(row['Timestamp'][8:10])
        trade_date_hour = int(row['Timestamp'][11:13])
        trade_date_minute = int(row['Timestamp'][14:16])
        trade_date_second = int(row['Timestamp'][17:19])

        self.trade_datetime = datetime.datetime(
            year=trade_date_year, month=trade_date_month,
            day=trade_date_day, hour=trade_date_hour,
            minute=trade_date_minute, second=trade_date_second
        )

        trade_type = row['Symbol'][14:15]

        if trade_type == 'C':
            self.trade_type = Trade.TYPE_CALL
        elif trade_type == 'P':
            self.trade_type = Trade.TYPE_PUT
        else:
            self.trade_type = None

        if row['Trade Action'].startswith('BUY'):
            self.action = Trade.ACTION_BUY
        elif row['Trade Action'].startswith('SELL'):
            self.action = Trade.ACTION_SELL
        elif row['Description'].endswith('OPTION EXPIRATION - EXPIRED'):
            self.action = Trade.ACTION_EXPIRATION
        else:
            self.action = None

    def __repr__(self):
        return '{self.trade_datetime}: {self.symbol} {type_display}{action_display} {self.strike} {self.expiration} $ {self.net_amount}'.format(
            self=self,
            action_display=self.get_action_display(),
            type_display=self.get_type_display()
        )

    def display(self, date_time):
        return '{self.symbol} {type_display:4} {self.strike} {expiration_display} [{quantity_display} @ {self.price:.2f}] $ {net_amount_display}'.format(
            self=self,
            expiration_display=self.get_expiration_display(date_time=date_time),
            net_amount_display=self.get_net_amount_display(),
            quantity_display=self.get_quantity_display(),
            type_display=self.get_type_display()
        )

    def get_action_display(self):
        return Trade.ACTION_DISPLAY.get(self.action) or ''

    def get_expiration_display(self, date_time):
        return '({} days)'.format((self.expiration - date_time.date()).days)

    def get_net_amount_display(self):
        if self.net_amount > 0:
            return '+{}'.format(self.net_amount)
        else:
            return '{}'.format(self.net_amount)

    def get_quantity_display(self):
        if self.quantity > 0:
            return '+{}'.format(self.quantity)
        else:
            return '{}'.format(self.quantity)

    def get_type_display(self):
        return Trade.TYPE_DISPLAY.get(self.trade_type) or ''


class ActivityImporter:
    @staticmethod
    def get_attibute(obj, attribute_name):
        if '.' in attribute_name:
            attribute_name, parts = attribute_name.split('.', 1)
            obj = getattr(obj, attribute_name)
            return ActivityImporter.get_attibute(obj=obj, attribute_name=parts)
        else:
            try:
                return getattr(obj, attribute_name)()
            except TypeError:
                return getattr(obj, attribute_name)

    @staticmethod
    def group_by(entries, attribute):
        key = lambda row: ActivityImporter.get_attibute(obj=row, attribute_name=attribute)

        return dict(
            [(value, list(items)) for value, items in itertools.groupby(entries, key)]
        )

    @staticmethod
    def sort_by(entries, attribute):
        key = lambda row: getattr(row, attribute)

        return sorted(entries, key=key)

    def __init__(self, filename):
        self.filename = filename
        self.trades = []

    def load(self):
        with open(self.filename, newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                if row['Type'] == 'TRADES':
                    if row['Symbol'].startswith('SPXW') and row['Net Amount'] != '$ 0.00':
                        self.trades.append(Trade(row=row))

    def display(self):
        trades = ActivityImporter.group_by(
            entries=self.trades, attribute='trade_datetime.date'
        )
        grand_total = 0

        for trade_day, day_entries in trades.items():
            print(trade_day)
            print(10 * '-')
            day_balance = 0

            day_entries = ActivityImporter.group_by(
                entries=day_entries, attribute='trade_datetime.time'
            )
            for trade_time, time_entries in day_entries.items():
                # Trade day
                print(trade_time)

                date_time = datetime.datetime(
                    year=trade_day.year, month=trade_day.month, day=trade_day.day,
                    hour=trade_time.hour, minute=trade_time.minute,
                    second=trade_time.second
                )

                time_entries = ActivityImporter.sort_by(entries=time_entries, attribute='strike')
                for time_entry in time_entries:
                    day_balance = day_balance + time_entry.net_amount

                    print('  ', time_entry.display(date_time=date_time))

                type_string = None
                if len(time_entries) == 2:
                    if time_entries[0].trade_type == Trade.TYPE_PUT and time_entries[1].trade_type == Trade.TYPE_PUT:
                        # 2 option put spread
                        if not(time_entries[0].strike == time_entries[1].strike):
                            # Discard same strike options

                            if time_entries[0].strike > time_entries[1].strike:
                                trade_smaller = time_entries[1]
                                trade_bigger = time_entries[0]
                            else:
                                trade_smaller = time_entries[0]
                                trade_bigger = time_entries[1]

                            if trade_smaller.action == Trade.ACTION_BUY and trade_bigger.action == Trade.ACTION_SELL:
                                if trade_smaller.position == Trade.POSITION_OPEN:
                                    type_string = 'Open put credit spread'
                                else:
                                    type_string = 'Close put debit spread'

                            elif trade_smaller.action == Trade.ACTION_SELL and trade_bigger.action == Trade.ACTION_BUY:
                                if trade_smaller.position == Trade.POSITION_OPEN:
                                    type_string = 'Open put debit spread'
                                else:
                                    type_string = 'Close put credit spread'
                    elif time_entries[0].trade_type == Trade.TYPE_CALL and time_entries[1].trade_type == Trade.TYPE_CALL:
                        # 2 option call spread
                        if not(time_entries[0].strike == time_entries[1].strike):
                            # Discard same strike options

                            if time_entries[0].strike > time_entries[1].strike:
                                trade_smaller = time_entries[1]
                                trade_bigger = time_entries[0]
                            else:
                                trade_smaller = time_entries[0]
                                trade_bigger = time_entries[1]

                            if trade_smaller.action == Trade.ACTION_BUY and trade_bigger.action == Trade.ACTION_SELL:
                                if trade_smaller.position == Trade.POSITION_OPEN:
                                    type_string = 'Open call debit spread'
                                else:
                                    type_string = 'Close call credit spread'

                            elif trade_smaller.action == Trade.ACTION_SELL and trade_bigger.action == Trade.ACTION_BUY:
                                if trade_smaller.position == Trade.POSITION_OPEN:
                                    type_string = 'Open call credit spread'
                                else:
                                    type_string = 'Close call debit spread'
                elif len(time_entries) == 4:
                    spread_trades = sorted(time_entries, key=lambda x: x.strike)
                    if spread_trades[0].trade_type == Trade.TYPE_PUT and spread_trades[0].action == Trade.ACTION_SELL:
                        if spread_trades[1].trade_type == Trade.TYPE_PUT and spread_trades[1].action == Trade.ACTION_BUY:
                            if spread_trades[2].trade_type == Trade.TYPE_CALL and spread_trades[2].action == Trade.ACTION_BUY:
                                if spread_trades[3].trade_type == Trade.TYPE_CALL and spread_trades[3].action == Trade.ACTION_SELL:
                                    if spread_trades[0].position == Trade.POSITION_CLOSE:
                                        type_string = 'Reverse iron condor'

                if type_string:
                    time_total = 0
                    for time_entry in time_entries:
                        time_total = time_total + time_entry.net_amount

                    print('   # {}: $ {}'.format(type_string, time_total))

            print('Day total: $ {}'.format(day_balance))
            grand_total = grand_total + day_balance

            print()

        print('Grand total: $ {}'.format(grand_total))

        print()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Import an APEX Clearing activity CSV file.')
    parser.add_argument(
        'filename', metavar='filename', type=str, help='path to the CSV file to import.'
    )

    args = parser.parse_args()
    instance = ActivityImporter(filename=args.filename)
    instance.load()
    instance.display()
